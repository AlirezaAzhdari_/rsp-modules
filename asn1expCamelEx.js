const qualifierRe =
  /(?:\.\&\w+)?[\w\s]*\([^()]*(?:\([^)]*\))*\)|DEFAULT[\w\s]+|{\w*\([^{}]*(?:\([^}]*\))*\)}/;
const postQualifierRe =
  /(?:\.\&\w+)?\([^()]*(?:\([^)]*\))*\)|{\w*\([^{}]*(?:\([^}]*\))*\)}/;

const types = [
  "OCTET STRING",
  "INTEGER",
  "NULL",
  "BOOLEAN",
  "NumericString",
  "SEQUENCE",
  "SET",
  "CHOICE",
  "BIT STRING",
  "ENUMERATED",
  "MAP-EXTENSION",
  "OBJECT IDENTIFIER",
  "IA5String",
  "EXTENSION",
  "UTF8String",
  "UTCTime",
  "GeneralizedTime",
];

const CLASSES = {
  UNIVERSAL: "UNIVERSAL",
  CONTEXT_SPECIFIC: "CONTEXT_SPECIFIC",
  APPLICATION: "APPLICATION",
  PRIVATE: "PRIVATE",
};

let capArgs = null;

function mixin(target, source) {
  for (var p in source) {
    if (source[p] !== undefined) {
      target[p] = source[p];
    }
  }

  return target;
}

function getBlockContents(s, startIdx) {
  let nesting = 0;
  let idx = startIdx;
  let ch = null;

  while (true) {
    ch = s[idx];

    switch (ch) {
      case "{":
        nesting++;
        break;

      case "}":
        if (!nesting) {
          throw new Error("Unmatched }");
        }

        if (nesting === 1) {
          return s.substring(startIdx + 1, idx);
        }

        nesting--;

        break;
    }

    idx++;

    if (idx === s.length) {
      throw new Error("Block not found");
    }
  }
}

function parseSequence(s) {
  const elements = [];

  let idx = 0;
  let match = null;
  let name = null;
  let element = null;
  let count = 0;

  while (true) {
    match = /^(\.{3}|[\w-]+\b)\s?/.exec(s.slice(idx));

    if (!match) {
      throw new Error("Identifier not found");
    }

    name = match[1];

    idx += match[0].length;

    if (name !== "...") {
      element = parseElement(s.slice(idx));

      count++;

      idx += element.length;
      delete element.length;

      let qualifiers = undefined;
      let optional = undefined;

      const qualifierRe = new RegExp(
        /^(QUALIFIER)?\s?(?:(\s(OPTIONAL)))?/.source.replace(
          "QUALIFIER",
          postQualifierRe.source
        )
      );

      match = qualifierRe.exec(s.slice(idx));

      if (match) {
        qualifiers = match[1];
        idx += match[0].length;
      }

      if (/^OPTIONAL/.test(s.slice(idx))) {
        optional = true;
        idx += "OPTIONAL".length;
      }

      if (element.ofElement) {
        if (element.ofElement.optional) {
          element.optional = true;
          delete element.ofElement.optional;
        }
      }

      const input = mixin(element, {
        name,
        qualifiers,
        optional,
      });

      elements.push(input);
    }

    if (s[idx] !== ",") {
      break;
    }

    idx++;
  }

  return elements;
}

function parseBitString(s) {
  return s
    .split(",")
    .filter((s) => s !== "...")
    .map((s) => {
      const match = /^([\w-]+)\((\d+)\)/.exec(s);

      if (!match) {
        throw new Error("Could not parse bit string value");
      }

      return {
        name: match[1],
        value: parseInt(match[2], 10),
      };
    });
}

function parseElement(s) {
  if (/^TRUE\b/.test(s)) {
    return {
      type: "BOOLEAN",
      value: true,
      length: "TRUE".length,
    };
  } else if (/^FALSE\b/.test(s)) {
    return {
      type: "BOOLEAN",
      value: false,
      length: "FALSE".length,
    };
  }

  const typeRe = new RegExp(
    /^(?:\[(\w*\s*\d+)\])?(?:(IMPLICIT)\s)?(TYPE)\s?(QUALIFIER)?(?:(\b(OF([\d])*\s*)))?/.source
      .replace("TYPE", types.join("|"))
      .replace("QUALIFIER", qualifierRe.source),
    "g"
  );

  // const typeRe = new RegExp(
  //   /^(?:\[(\w*\s*\d+)\])?(?:(IMPLICIT)\s)?(TYPE)\s?(QUALIFIER)?(?:\b(OF\s))?/.source
  //     .replace("TYPE", types.join("|"))
  //     .replace("QUALIFIER", qualifierRe.source),
  //   "g"
  // );

  // const typeRe = new RegExp(
  //   /^(?:\[(\w*\s*\d+)\])?(?:(IMPLICIT)\s)?(TYPE)\s?(QUALIFIER)?(?:(\b(OF\s(\[\d\])*)))?/.source
  //     .replace("TYPE", types.join("|"))
  //     .replace("QUALIFIER", qualifierRe.source),
  //   "g"
  // );

  let tag = undefined;
  let cls = undefined;
  let implicit = undefined;
  let type = undefined;
  let qualifiers = undefined;
  let ctorOf = undefined;
  let element = undefined;
  let length = 0;

  const match = typeRe.exec(s);

  if (match) {
    if (!match[1]) {
      // do nothing
    } else if (match[1].includes("APPLICATION")) {
      tag = match[1] && parseInt(match[1].replace("APPLICATION", ""), 10);
      cls = CLASSES.APPLICATION;
    } else if (match[1].includes("PRIVATE")) {
      //tag = match[1] && parseInt(match[1].replace(/(.*\s)(\d+)/, "$2"), 10);
      tag = match[1] && parseInt(match[1].replace("PRIVATE", ""), 10);
      cls = CLASSES.PRIVATE;
    } else {
      tag = match[1] && parseInt(match[1], 10);
      cls = CLASSES.CONTEXT_SPECIFIC;
    }

    implicit = match[2] && true;
    type = match[3];
    qualifiers = match[4];
    ctorOf = !!match[5];
    length = match[0].length;

    element = {
      name: undefined, // to hold first position in output
      tag,
      cls,
      implicit,
      type,
      qualifiers,
      length,
    };

    if (ctorOf) {
      const el = parseElement(s.slice(typeRe.lastIndex));
      element.ofElement = el;
      element.length += el.length;
      delete el.length;
    } else {
      let block = null;

      switch (type) {
        case "CHOICE":
        case "SEQUENCE":
        case "SET":
          block = getBlockContents(s, typeRe.lastIndex);
          element.length += block.length + 2;

          if (block !== "..." && block !== "") {
            element.elements = parseSequence(block);
          } else {
            element.elements = [];
          }
          break;

        case "BIT STRING":
        case "ENUMERATED":
          if (s[typeRe.lastIndex] === "{") {
            block = getBlockContents(s, typeRe.lastIndex);
            element.length += block.length + 2;
            element.values = parseBitString(block);
          }
          break;
        case "OBJECT IDENTIFIER":
          if (s[typeRe.lastIndex] === "{") {
            block = getBlockContents(s, typeRe.lastIndex);
            element.length += block.length + 2;
            element.qualifiers = block;
          }
          break;
        case "OCTET STRING":
          if (!/^\(?SIZE\b/.test(element.qualifiers)) {
            element.qualifiers = null;
            //element.length = "OCTET STRING".length;
          }

          break;
        default:
          break;
      }
    }

    element.qualifiers = resolveQualifier(element.qualifiers);

    if (!element.qualifiers) {
      element.qualifiers = undefined;
    }

    return element;
  } else {
    return parseSubtype(s);
  }
}

function parseSubtype(s, name) {
  // const argumentRe =
  //   /^(?:\[(\w*\s*\d+)\])?([\w-]+)\s?([\w-]+)?((?:DEFAULT\s?(?:{?[\w'\s]+}?))|(?:{[^{}]*(?:{[\w]+})*}))?\s?/g;

  const argumentRe =
    /^(?:\[(\w*\s*\d+)\])?([\w-]+)\s?([\w-]+)?((?:DEFAULT\s?(?:{?[\w'\s]+}?))|(?:{[^{}]*(?:{[\w]+})*})|(?:\s\w*\d*))?\s?/g;

  let matchArg = null;

  matchArg = argumentRe.exec(s);

  if (matchArg) {
    let arg = null;
    if (matchArg[2] === "IMPLICIT" || matchArg[2] === "EXPLICIT") {
      arg = findArg(matchArg[3]);
    } else {
      arg = findArg(matchArg[2]);
    }

    if (arg) {
      const elem = parseElement(arg);

      if (!matchArg[1]) {
        // do nothing
      } else if (matchArg[1].includes("APPLICATION")) {
        elem.tag =
          matchArg[1] && parseInt(matchArg[1].replace("APPLICATION", ""), 10);
        elem.cls = CLASSES.APPLICATION;
      } else if (matchArg[1].includes("PRIVATE")) {
        elem.tag =
          matchArg[1] && parseInt(matchArg[1].replace("PRIVATE", ""), 10);
        elem.cls = CLASSES.PRIVATE;
      } else {
        elem.tag = matchArg[1] && parseInt(matchArg[1], 10);

        elem.cls = CLASSES.CONTEXT_SPECIFIC;
      }

      if (
        matchArg[3] &&
        matchArg[3] !== "{bound}" &&
        matchArg[3] !== "OPTIONAL"
      ) {
        elem.qualifiers = resolveQualifier(matchArg[3]);
        if (matchArg[4]) {
          elem.qualifiers = elem.qualifiers.concat(matchArg[4]);
        }
      }

      if (matchArg[3] && matchArg[3] === "OPTIONAL") {
        elem.optional = true;
      }

      elem.length = argumentRe.lastIndex;

      if (!elem.qualifiers) {
        elem.qualifiers = undefined;
      }

      if (matchArg[2] === "IMPLICIT") {
        elem.implicit = true;
      }

      return elem;
    }
  }
  console.log({ name });
  console.log({ s });
  throw new Error("Could not find Subtype");
}

function resolveQualifier(qualifier) {
  let refinedQualifier = qualifier;
  if (!refinedQualifier) {
    return refinedQualifier;
  }

  // const boundRe = /\b(?:\(|\.\.)?([a-zA-Z&]+)/;
  // let match = null;

  // match = boundRe.exec(refinedQualifier);

  // while (match) {
  //   console.log({ match });
  //   if (paramsMap[match[1]]) {
  //     refinedQualifier = refinedQualifier.replace(
  //       `bound.${match[1]}`,
  //       paramsMap[match[1]]
  //     );
  //   } else {
  //     const constVarRe = new RegExp(
  //       /\b(?:VAR\sINTEGER)/.source.replace("VAR", match[1])
  //     );

  //     const varMatch = constVarRe.exec(capArgs);
  //     refinedQualifier = refinedQualifier.replace(match[1], varMatch[1]);
  //   }
  // }

  //console.log(refinedQualifier);

  return refinedQualifier;
}

function findArg(argName) {
  const argRe = new RegExp(
    /(?=[^-])\b(ARG)(?:{[\w-:]*})?\s/.source.replace("ARG", argName),
    "g"
  );

  let match = null;

  if ((match = argRe.exec(capArgs))) {
    return capArgs.slice(argRe.lastIndex);
  }
}

function getTypeName(s) {
  const argumentRe =
    /^(?:\[(\d+)\])?([\w-]+)\s?((?:DEFAULT\s?(?:{?[\w'\s]+}?))|(?:{[^{}]*(?:{[\w]+})*}))?\s?/g;

  let matchArg = null;

  matchArg = argumentRe.exec(s);
  return matchArg[2];
}

function naming(elements) {
  let definitions = {};

  for (let i = 0; i < elements.length; i++) {
    const { name, ...rest } = elements[i];

    definitions[`${name}`] = rest;
  }

  return definitions;
}

function automaticTagging(elements, count, tagging) {
  if (
    elements.type === "CHOICE" ||
    elements.type === "SEQUENCE" ||
    elements.type === "SET"
  ) {
    if (elements.elements) {
      elements = elements.elements;
    } else {
      elements = elements.ofElement;
    }
  }
  for (let i = 0; i < elements.length; i++) {
    switch (elements[i].type) {
      case "CHOICE":
      case "SEQUENCE":
      case "SET":
        if (tagging && elements[i].tag === undefined) {
          elements[i].tag = count;
          elements[i].cls = "CONTEXT_SPECIFIC";
        }
        count++;
        if (elements[i].elements) {
          automaticTagging(elements[i].elements, 0, true);
        } else {
          automaticTagging(elements[i].ofElement, 0, true);
        }

        break;
      default:
        if (tagging && elements[i].tag === undefined) {
          elements[i].tag = count;
          elements[i].cls = "CONTEXT_SPECIFIC";
        }
        count++;
        break;
    }
  }

  return elements;
}

function parse(s, args) {
  s = s
    .replace(/::=/g, "")
    .split("\n") // Split into row
    .filter((row) => !/^--/.test(row)) // Filter out comment rows
    .join(""); // Join rows

  s = s
    .replace(/\s+/g, " ") // Replace sequental whitespace with a single space
    .replace(/\B \b|\b \B|\B \B/g, ""); // Replace all space except between words

  args = args
    .replace(/::=/g, "")
    .split("\n") // Split into row
    .filter((row) => !/--/.test(row)) // Filter out comment rows
    .join(""); // Join rows

  capArgs = args
    .replace(/\s+/g, " ") // Replace sequental whitespace with a single space
    .replace(/\B \b|\b \B|\B \B/g, "") // Replace all space except between words
    .replace(/(.)(\[)/g, "$1 $2") // Add a space between any character and L bracket
    .replace("null", "");

  let blocks = {};

  let block = null;

  block = `SEQUENCE{${s}}`;
  block = parseElement(block);

  block.elements = automaticTagging(block.elements, 0, false);
  blocks = naming(block.elements);

  return { blocks };
}

module.exports = Object.freeze({
  parse,
});
